import java.io.File;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class CpuInfo {
        public static void main(String[] args) throws Exception {
                Source s = Source.newBuilder("llvm", new File("./cpuinfo.bc")).build();
                try (Context c = Context.newBuilder().allowAllAccess(true).build()) {
                        Value lib = c.eval(s);
                        Value fn = lib.getMember("getcpuinfo");
                        byte[] bytes = new byte[10000];
                        fn.executeVoid(bytes);
                        String str = new String(bytes);
                        HashMap<String, String> info = new HashMap<>();

                        int processors = 0;

                        for (String line : str.split("\n")) {
                                String[] parts = line.split(":");
                                if (parts.length > 1) {
                                        info.putIfAbsent(parts[0].trim(), parts[1].trim());
                                }
                        }

                        System.out.println(info.get("model name") + " " + info.get("cpu cores") + " cores");
                }
        }
}