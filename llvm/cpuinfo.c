#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void getcpuinfo(char result[]) {
        FILE* fp = fopen("/proc/cpuinfo", "r");

        fseek(fp, 0L, SEEK_END);
        long lSize = ftell(fp);
        rewind(fp);


        char str[10000] = {0};
        int n = fread(str, 10000, 1, fp);

        strcpy(result, str);
        fclose(fp);
}