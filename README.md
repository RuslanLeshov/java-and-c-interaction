# Java And C interaction
## JNI Bridge  
Загружает сишную библиотеку и выполняет метод  
Для сборки и запуска надо запустить:  
`javac -h CpuInfo.java`  
`gcc -fPIC -I"$JAVA_HOME/include" -I"$JAVA_HOME/include/linux" -shared -o libcpuinfo.so CpuInfo.c`  
`java -Djava.library.path=. HelloJNI`  

## LLVM  
Загружает биткод, сгенерированный clang, и выполняет его с помощью Polyglot API  
Для сборки и запуска запустить:  
`clang -g -c -O1 -emit-llvm cpuinfo.c`  
`javac CpuInfo.java`  
`java CpuInfo`  
