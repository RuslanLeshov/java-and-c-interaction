#include <jni.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "CpuInfo.h"

JNIEXPORT jstring JNICALL Java_CpuInfo_getCpuInfo(JNIEnv *env, jobject thisObj) {
	FILE* fp = fopen("/proc/cpuinfo", "r");

    fseek(fp, 0L, SEEK_END);
    long lSize = ftell(fp);
    rewind(fp);


    char str[10000] = {0};
    int n = fread(str, 10000, 1, fp);

    fclose(fp);

	return (*env)->NewStringUTF(env, str);
}

