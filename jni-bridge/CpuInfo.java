import java.util.HashMap;

public class CpuInfo {
	static {
		System.loadLibrary("cpuinfo");
	}

	private native String getCpuInfo();

	public static void main(String[] args) {
		String str = new CpuInfo().getCpuInfo();
		HashMap<String, String> info = new HashMap<>();

        int processors = 0;

        for (String line : str.split("\n")) {
            String[] parts = line.split(":");
            if (parts.length > 1) {
                info.putIfAbsent(parts[0].trim(), parts[1].trim());
            }
        }

        System.out.println(info.get("model name") + " " + info.get("cpu cores") + " cores");
	}
}